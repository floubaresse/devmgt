import React, { useEffect } from 'react';
import { JsonToTable } from 'react-json-to-table';

function DeviceTable() {

    const [myJson, setMyJson] = React.useState({ "state": "Idle" });

    // define the callAPI function that takes a first name and last name as parameters
    var callAPI = (e) => {

        console.log("In Call API *************");
        setMyJson({ "state": "Loading..." });
        // instantiate a headers object
        var myHeaders = new Headers();
        // add content type header to object
        myHeaders.append("Content-Type", "application/json");
        // using built in JSON utility package turn object to string and store in a variable
        var raw = JSON.stringify({});
        // create a JSON object with parameters for API call and store in a variable
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        // make API call with parameters and use promises to get response
        fetch("https://ll0m8uhyla.execute-api.us-west-2.amazonaws.com/dev", requestOptions)
            .then(response => response.json())
            .then(result => { console.log(result.body); setMyJson(result.body) })
    }

    // query db on load
    useEffect(() => {
        callAPI();
    }, [])

    return (

        < div className="App" >
            <button type="button" onClick={callAPI}>Refresh</button>
            <JsonToTable json={myJson} />
        </div >
    );
}

export default DeviceTable;